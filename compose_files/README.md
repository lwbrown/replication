# Compose Files

| Compose File    | Command to run                         | What it does                                |
|-----------------|----------------------------------------|---------------------------------------------|
| gitlab-ldap.yml | `docker-compose -f gitlab_ldap.yml up` | Deploys GitLab and LDAP, integrates the two |
| gitlab-saml.yml | `docker-compose -f gitlab_saml.yml up` | Deploys GitLab and SAML, integrates the two |


## Accessing the GitLab instance

You can access the GitLab instance via http://localhost

The root user credentials are:

| Username | Password   |
|----------|------------|
| root     | `password` |

You might see 502 errors at first. If so, give it a bit to run the initial crons and such and then refresh the page.

## Seeds

All GitLab images/builds are pre-seeded to help with replication. In them you should see:

* 27 users
* 3 groups (with 3 subgroups per group)
  * 10 labels per group/subgroup
* 26 keys
* 39 projects
  * 5 milestones per project
  * 5 labels per project
  * 5 issues per 
    * 1 comment per issue
* MRs
  * 1 comment per MR
* 50 personal snippets  
* 10 or so forks

If some are missing, it is not a big deal. A lot of random allocation is in place (permissions,
visibilities, etc.), so some seeds might have failed. Still should present a decent data set
to work with.

## LDAP/SAML users

| Username | GitLab Password | LDAP/SMAL Password | Email |
|----------|-----------------|--------------------|-------|
| alice | password | password | alice@example.com |
| bob | password | password | bob@example.com |
| charles | password | password | charles@example.com |
| dylan | password | password | dylan@example.com |
| ethan | password | password | ethan@example.com |
| fred | password | password | fred@example.com |
| george | password | password | george@example.com |
| heather | password | password | heather@example.com |
| inga | password | password | inga@example.com |
| jacob | password | password | jacob@example.com |
| kyle | password | password | kyle@example.com |
| lisa | password | password | lisa@example.com |
| mike | password | password | mike@example.com |
| nate | password | password | nate@example.com |
| olivia | password | password | olivia@example.com |
| penelope | password | password | penelope@example.com |
| quincy | password | password | quincy@example.com |
| rachel | password | password | rachel@example.com |
| steve | password | password | steve@example.com |
| todd | password | password | todd@example.com |
| ulysses | password | password | ulysses@example.com |
| vivian | password | password | vivian@example.com |
| wayne | password | password | wayne@example.com |
| xavier | password | password | xavier@example.com |
| yvonne | password | password | yvonne@example.com |
| zach | password | password | zach@example.com |
