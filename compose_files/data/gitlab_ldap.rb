gitlab_rails['ldap_enabled'] = true
gitlab_rails['ldap_servers'] = YAML.load <<-EOS
  main:
    label: 'LDAP'
    host: 'ldap'
    port: 389
    uid: 'uid'
    encryption: 'plain'
    bind_dn: 'cn=admin,dc=example,dc=com'
    password: 'admin'
    base: 'dc=example,dc=com'
EOS
