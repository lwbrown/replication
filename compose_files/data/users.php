<?php



$config = array(

    'test-sp' => array(
        'saml:SP',
        'entityID' => 'http://app.example.com',
        'idp' => 'http://localhost:8080/simplesaml/saml2/idp/metadata.php',
    ),

    'admin' => array(
        'core:AdminPassword',
    ),

    'example-userpass' => array(
	'exampleauth:UserPass',
        'alice:password' => array(
            'uid' => array('1'),
            'eduPersonAffiliation' => array('admin'),
            'email' => 'alice@example.com',
            'first_name' => 'Alice',
            'last_name' => 'McAlice',
            'name' => 'Alice McAlice',
            'description' => 'Alice is a test user'
        ),
        'bob:password' => array(
            'uid' => array('2'),
            'eduPersonAffiliation' => array('dev'),
            'email' => 'bob@example.com',
            'first_name' => 'Bob',
            'last_name' => 'McBob',
            'fullName' => 'Bob McBob',
            'description' => 'Bob is a test user'
        ),
        'charles:password' => array(
            'uid' => array('3'),
            'eduPersonAffiliation' => array('qa'),
            'email' => 'charles@example.com',
            'first_name' => 'Charles',
            'last_name' => 'McCharles',
            'fullName' => 'Charles McCharles',
            'description' => 'Charles is a test user'
        ),
        'dylan:password' => array(
            'uid' => array('4'),
            'eduPersonAffiliation' => array('support'),
            'email' => 'dylan@example.com',
            'first_name' => 'Dylan',
            'last_name' => 'McDylan',
            'fullName' => 'Dylan McDylan',
            'description' => 'Dylan is a test user'
        ),
        'ethan:password' => array(
            'uid' => array('5'),
            'eduPersonAffiliation' => array('admin'),
            'email' => 'ethan@example.com',
            'first_name' => 'Ethan',
            'last_name' => 'McEthan',
            'fullName' => 'Ethan McEthan',
            'description' => 'Ethan is a test user'
        ),
        'fred:password' => array(
            'uid' => array('6'),
            'eduPersonAffiliation' => array('dev'),
            'email' => 'fred@example.com',
            'first_name' => 'Fred',
            'last_name' => 'McFred',
            'fullName' => 'Fred McFred',
            'description' => 'Fred is a test user'
        ),
        'george:password' => array(
            'uid' => array('7'),
            'eduPersonAffiliation' => array('qa'),
            'email' => 'george@example.com',
            'first_name' => 'George',
            'last_name' => 'McGeorge',
            'fullName' => 'George McGeorge',
            'description' => 'George is a test user'
        ),
        'heather:password' => array(
            'uid' => array('8'),
            'eduPersonAffiliation' => array('support'),
            'email' => 'heather@example.com',
            'first_name' => 'Heather',
            'last_name' => 'McHeather',
            'fullName' => 'Heather McHeather',
            'description' => 'Heather is a test user'
        ),
        'inga:password' => array(
            'uid' => array('9'),
            'eduPersonAffiliation' => array('admin'),
            'email' => 'inga@example.com',
            'first_name' => 'Inga',
            'last_name' => 'McInga',
            'fullName' => 'Inga McInga',
            'description' => 'Inga is a test user'
        ),
        'jacob:password' => array(
            'uid' => array('10'),
            'eduPersonAffiliation' => array('dev'),
            'email' => 'jacob@example.com',
            'first_name' => 'Jacob',
            'last_name' => 'McJacob',
            'fullName' => 'Jacob McJacob',
            'description' => 'Jacob is a test user'
        ),
        'kyle:password' => array(
            'uid' => array('11'),
            'eduPersonAffiliation' => array('qa'),
            'email' => 'kyle@example.com',
            'first_name' => 'Kyle',
            'last_name' => 'McKyle',
            'fullName' => 'Kyle McKyle',
            'description' => 'Kyle is a test user'
        ),
        'lisa:password' => array(
            'uid' => array('12'),
            'eduPersonAffiliation' => array('support'),
            'email' => 'lisa@example.com',
            'first_name' => 'Lisa',
            'last_name' => 'McLisa',
            'fullName' => 'Lisa McLisa',
            'description' => 'Lisa is a test user'
        ),
        'mike:password' => array(
            'uid' => array('13'),
            'eduPersonAffiliation' => array('admin'),
            'email' => 'mike@example.com',
            'first_name' => 'Mike',
            'last_name' => 'McMike',
            'fullName' => 'Mike McMike',
            'description' => 'Mike is a test user'
        ),
        'nate:password' => array(
            'uid' => array('14'),
            'eduPersonAffiliation' => array('dev'),
            'email' => 'nate@example.com',
            'first_name' => 'Nate',
            'last_name' => 'McNate',
            'fullName' => 'Nate McNate',
            'description' => 'Nate is a test user'
        ),
        'olivia:password' => array(
            'uid' => array('15'),
            'eduPersonAffiliation' => array('qa'),
            'email' => 'olivia@example.com',
            'first_name' => 'Olivia',
            'last_name' => 'McOlivia',
            'fullName' => 'Olivia McOlivia',
            'description' => 'Olivia is a test user'
        ),
        'penelope:password' => array(
            'uid' => array('16'),
            'eduPersonAffiliation' => array('support'),
            'email' => 'penelope@example.com',
            'first_name' => 'Penelope',
            'last_name' => 'McPenelope',
            'fullName' => 'Penelope McPenelope',
            'description' => 'Penelope is a test user'
        ),
        'quincy:password' => array(
            'uid' => array('17'),
            'eduPersonAffiliation' => array('admin'),
            'email' => 'quincy@example.com',
            'first_name' => 'Quincy',
            'last_name' => 'McQuincy',
            'fullName' => 'Quincy McQuincy',
            'description' => 'Quincy is a test user'
        ),
        'rachel:password' => array(
            'uid' => array('18'),
            'eduPersonAffiliation' => array('dev'),
            'email' => 'rachel@example.com',
            'first_name' => 'Rachel',
            'last_name' => 'McRachel',
            'fullName' => 'Rachel McRachel',
            'description' => 'Rachel is a test user'
        ),
        'steve:password' => array(
            'uid' => array('19'),
            'eduPersonAffiliation' => array('qa'),
            'email' => 'steve@example.com',
            'first_name' => 'Steve',
            'last_name' => 'McSteve',
            'fullName' => 'Steve McSteve',
            'description' => 'Steve is a test user'
        ),
        'todd:password' => array(
            'uid' => array('20'),
            'eduPersonAffiliation' => array('support'),
            'email' => 'todd@example.com',
            'first_name' => 'Todd',
            'last_name' => 'McTodd',
            'fullName' => 'Todd McTodd',
            'description' => 'Todd is a test user'
        ),
        'ulysses:password' => array(
            'uid' => array('21'),
            'eduPersonAffiliation' => array('admin'),
            'email' => 'ulysses@example.com',
            'first_name' => 'Ulysses',
            'last_name' => 'McUlysses',
            'fullName' => 'Ulysses McUlysses',
            'description' => 'Ulysses is a test user'
        ),
        'vivian:password' => array(
            'uid' => array('22'),
            'eduPersonAffiliation' => array('dev'),
            'email' => 'vivian@example.com',
            'first_name' => 'Vivian',
            'last_name' => 'McVivian',
            'fullName' => 'Vivian McVivian',
            'description' => 'Vivian is a test user'
        ),
        'wayne:password' => array(
            'uid' => array('23'),
            'eduPersonAffiliation' => array('qa'),
            'email' => 'wayne@example.com',
            'first_name' => 'Wayne',
            'last_name' => 'McWayne',
            'fullName' => 'Wayne McWayne',
            'description' => 'Wayne is a test user'
        ),
        'xavier:password' => array(
            'uid' => array('24'),
            'eduPersonAffiliation' => array('support'),
            'email' => 'xavier@example.com',
            'first_name' => 'Xavier',
            'last_name' => 'McXavier',
            'fullName' => 'Xavier McXavier',
            'description' => 'Xavier is a test user'
        ),
        'yvonne:password' => array(
            'uid' => array('25'),
            'eduPersonAffiliation' => array('admin'),
            'email' => 'yvonne@example.com',
            'first_name' => 'Yvonne',
            'last_name' => 'McYvonne',
            'fullName' => 'Yvonne McYvonne',
            'description' => 'Yvonne is a test user'
        ),
        'zach:password' => array(
            'uid' => array('26'),
            'eduPersonAffiliation' => array('dev'),
            'email' => 'zach@example.com',
            'first_name' => 'Zach',
            'last_name' => 'McZach',
            'fullName' => 'Zach McZach',
            'description' => 'Zach is a test user'
        ),
    ),
);
