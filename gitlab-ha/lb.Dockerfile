FROM ubuntu:latest
RUN apt-get update
RUN apt-get install -y haproxy curl
COPY data/lb/haproxy.cfg /etc/haproxy/haproxy.cfg
COPY scripts/lb/ /assets/
ENTRYPOINT /assets/wrapper
