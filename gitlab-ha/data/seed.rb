#!/opt/gitlab/embedded/bin/ruby

users = [
  'alice', 'bob', 'charles', 'dylan',
  'ethan', 'fred', 'george', 'heather',
  'inga', 'jacob', 'kyle', 'lisa',
  'mike', 'nate', 'olivia', 'penelope',
  'quincy', 'rachel', 'steve', 'todd',
  'ulysses', 'vivian', 'wayne', 'xavier',
  'yvonne', 'zach'
]
groups = ['dev', 'qa', 'support']
subgroups = ['tier1', 'tier2', 'tier3']
projects = [
  'https://github.com/reyloc/slack-wrapper.git',
  'https://github.com/torvalds/linux.git',
  'https://gitlab.gnome.org/GNOME/gimp.git',
  'https://gitlab.gnome.org/GNOME/gnome-mud.git',
  'https://gitlab.com/fdroid/fdroidclient.git',
  'https://gitlab.com/inkscape/inkscape.git',
  'https://github.com/gnachman/iTerm2.git',
  'https://gitlab.com/gitlab-org/gitlab-test.git',
  'https://gitlab.com/gitlab-org/gitlab-shell.git',
  'https://gitlab.com/gnuwget/wget2.git',
  'https://gitlab.com/Commit451/LabCoat.git',
  'https://github.com/jashkenas/underscore.git',
  'https://github.com/flightjs/flight.git',
  'https://github.com/twitter/typeahead.js.git',
  'https://github.com/h5bp/html5-boilerplate.git',
  'https://github.com/google/material-design-lite.git',
  'https://github.com/jlevy/the-art-of-command-line.git',
  'https://github.com/FreeCodeCamp/freecodecamp.git',
  'https://github.com/google/deepdream.git',
  'https://github.com/jtleek/datasharing.git',
  'https://github.com/WebAssembly/design.git',
  'https://github.com/airbnb/javascript.git',
  'https://github.com/tessalt/echo-chamber-js.git',
  'https://github.com/atom/atom.git',
  'https://github.com/mattermost/mattermost-server.git',
  'https://github.com/purifycss/purifycss.git',
  'https://github.com/facebook/nuclide.git',
  'https://github.com/wbkd/awesome-d3.git',
  'https://github.com/kilimchoi/engineering-blogs.git',
  'https://github.com/gilbarbara/logos.git',
  'https://github.com/reduxjs/redux.git',
  'https://github.com/awslabs/s2n.git',
  'https://github.com/arkency/reactjs_koans.git',
  'https://github.com/twbs/bootstrap.git',
  'https://github.com/chjj/ttystudio.git',
  'https://github.com/MostlyAdequate/mostly-adequate-guide.git',
  'https://github.com/octocat/Spoon-Knife.git',
  'https://github.com/opencontainers/runc.git',
  'https://github.com/googlesamples/android-topeka.git'
]
labels = [
  'Sun',
  'Mercury',
  'Venus',
  'Earth',
  'Mars',
  'Jupiter',
  'Saturn',
  'Uranus',
  'Neptune',
  'Pluto'
]
issues = [
  'do a thing',
  'do another thing',
  'do yet another thing',
  'keep doing things',
  'keep doing more things'
]
def determine_owner(users, i)
  user = users[i]
  if user.nil?
    user = users.sample
  end
  user = User.find_by_username(user)
  namespace = user.namespaces.sample
  unless namespace.type.nil?
    if Group.find(namespace.id).members.where(user_id: user.id).first.access_level > 20
      return user, namespace.id
    end
  end
  return user, namespace.id
end
def determine_author(p)
  users = p.members.where('access_level > 10')
  unless users.count.zero?
    users.sample.user_id
  end
  p.creator_id
end
puts "Seeding users"
users.each do |user|
  begin
    User.create!(
      username: user,
      name: "#{user.capitalize} Mc#{user.capitalize}",
      email: "#{user}@example.com",
      confirmed_at: DateTime.now,
      password: 'password'
    )
    print '.'
  rescue ActiveRecord::RecordInvalid
    print 'F'
  end
end
puts
puts "Changing root password to password"
u = User.first
u.password = 'password'
u.password_confirmation = 'password'
u.save!
puts "Seeding keys for users"
users.each_with_index do |user,index|
  key = "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAIEAiPWx6WM4lhHNedGfBpPJNPpZ7yKu+dnn1SJejgt#{index + 101}6k6YjzGGphH2TUxwKzxcKDKKezwkpfnxPkSMkuEspGRt/aZZ9wa++Oi7Qkr8prgHc4soW6NUlfDzpvZK2H5E7eQaSeP3SAwGmQKUFHCddNaP0L+hM7zhFNzjFvpaMgJw0="
  begin
    Key.create!(
      user_id: index+1,
      created_at: DateTime.now,
      key: key,
      title: "Sample key for #{user}"
    )
    print '.'
  rescue ActiveRecord::RecordInvalid
    print 'F'
  end
end
puts
puts 'Seeding groups'
groups.each do |group|
  begin
    Group.create!(
      name: group.titleize,
      path: group,
      owner_id: 1,
      created_at: DateTime.now,
      type: 'Group',
      description: "Description for #{group} group"
    )
    print '.'
  rescue ActiveRecord::RecordInvalid
    print 'F'
  end
end
puts
puts "Seeding subgroups"
subgroups.each do |subgroup|
  groups.each do |group|
    begin
      Group.create!(
        name: "#{group.titleize}-#{subgroup.titleize}",
        path: "#{group}-#{subgroup}",
        owner_id: 1,
        created_at: DateTime.now,
        type: 'Group',
        description: "Subgroup for #{group}",
	parent_id: Group.find_by_name(group.titleize)
      )
      print '.'
    rescue ActiveRecord::RecordInvalid
      print 'F'
    end
  end
end
puts
puts "Assigning users to groups and subgroups"
users.each_with_index do |user,index|
  case index % 4
    when 0
      Group.find_by_name('Dev').add_user(index+1, Gitlab::Access.values.sample)
    when 2
      Group.find_by_name('Qa').add_user(index+1, Gitlab::Access.values.sample)
    when 3
      Group.find_by_name('Support').add_user(index+1, Gitlab::Access.values.sample)
  end
  Group.find_by_name("#{groups.sample.titleize}-#{subgroups.sample.titleize}").add_user(index+1, Gitlab::Access.values.sample)
end
puts "Seeding projects"
projects.each_with_index do |project, index|
  user, namespace_id = determine_owner(users, index)
  params = {
    name: project.split('/').last.chomp('.git'),
    description: 'Lorem ipsum',
    import_url: project,
    namespace_id: namespace_id,
    visibility_level: Gitlab::VisibilityLevel.values.sample,
    skip_disk_validation: true
  }
  Sidekiq::Worker.skipping_transaction_check do
    project = Projects::CreateService.new(user, params).execute
  end
  print '.'
end
puts
puts "Seeding project milestones"
Project.all.each do |project|
  5.times do |i|
    params = {
      title: "v#{i}.0",
      description: 'Lorem ipsum',
      state: [:active, :closed].sample
    }
    milestone = Milestones::CreateService.new(
      project, project.team.users.sample, params).execute
  end
  print '.'
end
puts
puts "Seeding labels"
labels.each_with_index do |label, index|
  Group.all.each do |group|
    Labels::CreateService
      .new(title: label, color: "##{Digest::MD5.hexdigest(label)[0..5]}")
      .execute(group: @group)
  end
  if index.even?
    Project.all.each do |project|
      Labels::CreateService
        .new(title: label, color: "##{Digest::MD5.hexdigest(label)[0..5]}")
        .execute(project: @project)
    end
  end
  print '.'
end
puts
puts "Seeding issues"
Project.all.each do |project|
  issues.each do |issue|
    begin
      author = determine_author(project)
      Issue.create!(
        title: issue.titleize,
	description: "You really should #{issue}",
	state: [:closed, :opened].sample,
	weight: [nil,1,2,3,4,5,6,7,8,9,10].sample,
	created_at: DateTime.now,
	author_id: author,
	confidential: [true, false].sample,
	project_id: project.id
      )
      print '.'
    rescue ActiveRecord::RecordInvalid
      print 'F'
    end
  end
end
puts
puts "Seeding MRs"
Project.non_archived.with_merge_requests_enabled.reject(&:empty_repo?).each_with_index do |project,index|
  branches = project.repository.branch_names.sample(10)
  branches.each do |branch_name|
    break if branches.size < 2
    source_branch = branches.pop
    target_branch = branches.pop
    label_ids = project.labels.pluck(:id).sample(3)
    label_ids += project.group.labels.sample(3) if project.group
    params = {
      source_branch: source_branch,
      target_branch: target_branch,
      title: "Test Merge Request #{project.id}-#{index}",
      description: "Lorem ipsum",
      milestone: project.milestones.sample,
      assignees: [project.team.users.sample],
      label_ids: label_ids
    }
    developer = project.team.developers.sample
    break unless developer
    Sidekiq::Worker.skipping_transaction_check do
      MergeRequests::CreateService.new(project, developer, params).execute
    rescue Repository::AmbiguousRefError
    end
    print '.'
  end
end
puts
puts "Seeding personal snippets"
50.times do |i|
  user = User.all.sample
  begin
    PersonalSnippet.create!(
      author_id: user.id,
      title: "Personal Snipper #{i}",
      file_name:  "some_file_#{i}.rb",
      visibility_level: Gitlab::VisibilityLevel.values.sample,
      content: "#{['echo', 'puts', 'print'].sample} 'Hello World'",
    )
    print '.'
  rescue ActiveRecord::RecordInvalid
    print 'F'
  end
end
puts
puts "Seeding issue comments"
Issue.find_each do |issue|
  project = issue.project
  user = project.team.users.sample
  params = {
    noteable_type: 'Issue',
    noteable_id: issue.id,
    note: 'Lorem ipsum'
  }
  Notes::CreateService.new(project, user, params).execute
  print '.'
end
puts
puts "Seeding MR comments"
MergeRequest.find_each do |mr|
  project = mr.project
  user = project.team.users.sample
  params = {
    noteable_type: 'MergeRequest',
    noteable_id: mr.id,
    note: 'Lorem ipsum'
  }
  Notes::CreateService.new(project, user, params).execute
  print '.'
end
puts
puts "Seeding protected branches"
Project.all.each do |project|
  params = {
    name: 'master'
  }
  ProtectedBranches::CreateService.new(project, User.first, params).execute
  print '.'
end
puts
puts "Seeding forks"
User.all.sample(10).each do |user|
  source_project = Project.public_only.sample
  return unless source_project
  fork_project = Projects::ForkService.new(source_project, user, namespace: user.namespace).execute
  if fork_project.valid?
    print '.'
  else
    print 'F'
  end
end
puts
