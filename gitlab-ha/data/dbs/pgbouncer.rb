roles ['pgbouncer_role']
pgbouncer['admin_users'] = %w(pgbouncer gitlab-consul)
consul['watchers'] = %w(postgresql)
pgbouncer['users'] = {
  'gitlab-consul': {
    password: 'ad16e50f157234626af7ffc258ddf986'
  },
  'pgbouncer': {
    password: 'a45753afaca0db833a6f7c7b2864b9d9'
  }
}
consul['node_name'] = 'pgbouncer'
consul['configuration'] = {
  bind_addr: '0.0.0.0',
  retry_join: %w(consul1 consul2 consul3)
}
