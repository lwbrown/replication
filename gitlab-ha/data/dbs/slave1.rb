roles ['postgres_role']
postgresql['listen_address'] = '0.0.0.0'
postgresql['hot_standby'] = 'on'
postgresql['wal_level'] = 'replica'
postgresql['shared_preload_libraries'] = 'repmgr_funcs'
gitlab_rails['auto_migrate'] = false
consul['services'] = %w(postgresql)
postgresql['pgbouncer_user_password'] = 'b7b82b4fce2aac079878e614483158da'
postgresql['sql_user_password'] = '1231d876964a583791877d7cb9b6ac39'
postgresql['max_wal_senders'] = 4
postgresql['max_replication_slots'] = 4
postgresql['trust_auth_cidr_addresses'] = %w(127.0.0.1/32 172.0.0.0/8 10.0.0.0/8 192.168.0.0/16)
repmgr['trust_auth_cidr_addresses'] = %w(127.0.0.1/32 172.0.0.0/8 10.0.0.0/8 192.168.0.0/16)
consul['node_name'] = 'pg2'
consul['configuration'] = {
  bind_addr: '0.0.0.0',
  retry_join: %w(consul1 consul2 consul3)
}
repmgr['master_on_initialization'] = false
