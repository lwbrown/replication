roles ['consul_role']
consul['node_name'] = 'consul3'
consul['configuration'] = {
  server: true,
  bind_addr: '0.0.0.0',
  retry_join: %w(consul1 consul2 consul3)
}
gitlab_rails['auto_migrate'] = false
