roles ['redis_slave_role', 'redis_sentinel_role']
redis['bind'] = '0.0.0.0'
redis['port'] = 6379
redis['password'] = 'password'
redis['master_ip'] = 'MASTERIP'
gitlab_rails['auto_migrate'] = false
sentinel['bind'] = '0.0.0.0'
sentinel['quorum'] = 2
