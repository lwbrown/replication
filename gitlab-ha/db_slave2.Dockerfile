FROM ubuntu:latest
RUN apt-get update
RUN apt-get install -y curl openssh-server ca-certificates tzdata
RUN curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | bash
RUN apt-get install -y gitlab-ee
COPY scripts/dbs/slave/ /assets/
COPY data/dbs/slave2.rb /etc/gitlab/gitlab.rb
COPY data/gitlab-secrets.json /etc/gitlab/gitlab-secrets.json
RUN /assets/setup
ENTRYPOINT /assets/wrapper
