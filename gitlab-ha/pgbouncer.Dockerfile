FROM ubuntu:latest
RUN apt-get update
RUN apt-get install -y curl openssh-server ca-certificates tzdata
RUN curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | bash
RUN apt-get install -y gitlab-ee
COPY scripts/dbs/pgbouncer /assets/
COPY data/dbs/pgbouncer.rb /etc/gitlab/gitlab.rb
COPY data/gitlab-secrets.json /etc/gitlab/gitlab-secrets.json
COPY data/dbs/pg_auth /var/opt/gitlab/pgbouncer/pg_auth
RUN /assets/setup
ENTRYPOINT /assets/wrapper
